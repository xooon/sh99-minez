package Economy.Configuration;

import config.IConfigurable;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class EconomyConfiguration implements IConfigurable
{
        @Override
        public String getName() {
            return "economy";
        }

        @Override
        public FileConfiguration getConfiguration() {
            FileConfiguration config = new YamlConfiguration();

            List<HashMap<String, Object>> items = new ArrayList<>();
            //TODO: protect players kill their own animals (combine with custom world guard block protection) check if block is protected
            config.set("currency", "€");
            config.set("incoming.block.break", 0.1);
            config.set("incoming.block.place", 0.1);
            config.set("incoming.entity.kill.default", 0.1);
            config.set("incoming.entity.kill.player", 10);
            config.set("incoming.entity.kill.monster", 5);
            config.set("incoming.entity.kill.living", 5);
            config.set("balance.beginning", 0.00);

            return config;
        }

}