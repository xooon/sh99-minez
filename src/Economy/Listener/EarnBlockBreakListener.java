package Economy.Listener;

import Economy.Storage.EconomyPeristanceStorage;
import minez.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.Set;

public class EarnBlockBreakListener implements Listener
{

    private EconomyPeristanceStorage peristanceStorage;

    private FileConfiguration configuration;

    public EarnBlockBreakListener(EconomyPeristanceStorage peristanceStorage, FileConfiguration configuration)
    {
        this.peristanceStorage = peristanceStorage;
        this.configuration = configuration;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onBlockBreak(BlockBreakEvent event)
    {
        Player player = event.getPlayer();
        YamlConfiguration config = this.peristanceStorage.get(player);

        if(null == config){
            return;
        }

        double balance = 0.1;
        double incoming = 0.1;

        if(null != this.configuration.get("incoming.block.break")){
            balance = ((double) this.configuration.get("incoming.block.break"));
        }

        if(null != config.get("balance")){
            balance = ((double) config.get("balance"))+incoming;
        }

        config = this.peristanceStorage.store(player, balance);

        if(null == config){
            player.sendMessage(ChatColor.RED + "There was a problem while persisting player economy data, please try again or contact our support.");
            return;
        }
    }
}
