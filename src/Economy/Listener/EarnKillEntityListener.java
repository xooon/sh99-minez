package Economy.Listener;

import Economy.Storage.EconomyPeristanceStorage;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;

public class EarnKillEntityListener implements Listener
{

    private EconomyPeristanceStorage peristanceStorage;

    private FileConfiguration configuration;

    public EarnKillEntityListener(EconomyPeristanceStorage peristanceStorage, FileConfiguration configuration)
    {
        this.peristanceStorage = peristanceStorage;
        this.configuration = configuration;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public void onEntityDeath(EntityDeathEvent event)
    {
        if(!(event.getEntity().getKiller() instanceof Player)){
            return;
        }

        Player player = event.getEntity().getKiller();
        YamlConfiguration config = this.peristanceStorage.get(player);

        if(null == config){
            return;
        }

        double balance = 0.1;
        double incoming = 0.1;

        if(event.getEntity() instanceof Ageable){
            incoming += 1;
        }else if (event.getEntity() instanceof Monster){
            incoming += 5;
        }else if(event.getEntity() instanceof Player){
            incoming += 10;
        }

        if(null != this.configuration.get("incoming.entity.kill.default")){
            balance = ((double) this.configuration.get("incoming.entity.kill.default"));
        }

        if(null != config.get("balance")){
            balance = ((double) config.get("balance"))+incoming;
        }

        config = this.peristanceStorage.store(player, balance);

        if(null == config){
            player.sendMessage(ChatColor.RED + "There was a problem while persisting player economy data, please try again or contact our support.");
            return;
        }
    }
}
