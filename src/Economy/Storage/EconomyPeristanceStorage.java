package Economy.Storage;

import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class EconomyPeristanceStorage
{
    private JavaPlugin plugin;

    private FileConfiguration configuration;

    public EconomyPeristanceStorage(JavaPlugin plugin, FileConfiguration configuration)
    {
        this.plugin = plugin;
        this.configuration = configuration;
    }

    public YamlConfiguration add(Player player, double amount){
        YamlConfiguration config = this.get(player);

        if(null == config){
            return null;
        }

        if(null == config.get("balance")){
            return null;
        }

        double balance = (double) config.get("balance");

        balance += amount;

        config = this.store(player, balance);

        if(null == config){
            return null;
        }

        return config;
    }

    public YamlConfiguration store(Player player, double balance) {
        File file = new File(this.plugin.getDataFolder()+ "/Economy/persistence/", player.getUniqueId().toString()+".yml");
        YamlConfiguration configuration = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        try {
            if(!file.exists()){
                file.createNewFile();
                configuration.set("name", player.getName());
                configuration.set("uuid", player.getUniqueId().toString());
                configuration.set("balance", this.configuration.get("balance.beginning"));
                configuration.save(file);
            }
            configuration.load(file);
        } catch (Exception e) {
            return null;
        }

        configuration.set("balance", balance);
        try {
            configuration.save(file);
        } catch (IOException e) {
            return null;
        }

        return configuration;
    }


    public YamlConfiguration get(Player player) {
        File file = new File(this.plugin.getDataFolder()+ "/Economy/persistence/", player.getUniqueId().toString()+".yml");
        YamlConfiguration configuration = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        try {
            if(!file.exists()){
                file.createNewFile();
                configuration.set("name", player.getName());
                configuration.set("uuid", player.getUniqueId().toString());
                configuration.set("balance", this.configuration.get("balance.beginning"));
                configuration.save(file);
            }
            configuration.load(file);
        } catch (Exception e) {
            return null;
        }

        return configuration;
    }
}
