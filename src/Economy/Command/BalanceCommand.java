package Economy.Command;

import Economy.Storage.EconomyPeristanceStorage;
import Economy.TabCompleter.Balance;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

public class BalanceCommand implements CommandExecutor
{
    private EconomyPeristanceStorage peristanceStorage;

    private FileConfiguration configuration;

    public BalanceCommand(EconomyPeristanceStorage peristanceStorage, FileConfiguration configuration)
    {
        this.peristanceStorage = peristanceStorage;
        this.configuration = configuration;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)){
            return false;
        }

        Player player = (Player) sender;

        double balance = 0.00;

        YamlConfiguration config = this.peristanceStorage.get(player);

        if(null == config){
            player.sendMessage(ChatColor.RED + "There was a problem while persisting player economy data, please try again or contact our support.");
            return true;
        }

        if(null != config.get("balance")){
            balance = (double) config.get("balance");
        }

        sender.sendMessage(ChatColor.WHITE + "Balance: " + ChatColor.GREEN + ((double) Math.round(balance*100)/100) + " " + this.configuration.get("currency") + ChatColor.WHITE + ".");

        return true;
    }
}
