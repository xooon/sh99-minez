package minez;

import Block.Command.PlotCommand;
import Block.Storage.BlockPersistanceStorage;
import Block.Storage.ChunkPersistanceStorage;
import Block.TabCompleter.PlotTabCompleter;
import Economy.Command.BalanceCommand;
import Economy.Configuration.EconomyConfiguration;
import Economy.Listener.EarnBlockBreakListener;
import Economy.Listener.EarnBlockPlaceListener;
import Economy.Listener.EarnKillEntityListener;
import Economy.Storage.EconomyPeristanceStorage;
import config.DefaultConfiguration;
import config.IConfigurable;
import Block.Configuration.BlockProtectionConfiguration;
import Block.Initialiser.BlockMetadataInitialiser;
import minez.Back.BackCommand;
import minez.Catch.Configuration.CatchEntityConfiguration;
import minez.Drop.Listener.BlockDropListener;
import Block.Listener.Block.BlockBreakProtectionListener;
import Block.Listener.Block.BlockPlaceRegistrationListener;
import Block.Listener.Block.BlockPlayerInteractionListener;
import minez.Catch.Listener.CatchEntityListener;
import minez.Drop.Listener.BlockSharedLootListener;
import minez.Home.Command.HomeCommand;
import LivingEnvironment.Listener.SignEntityListener;
import minez.Near.Command.NearCommand;
import minez.Notify.Listener.TagNotifyListener;
import minez.Player.Configuration.StarterKitConfiguration;
import minez.Player.Listener.PlayerDeathListener;
import minez.Player.Listener.PlayerInitialiserListener;
import minez.RandomTeleport.Command.RandomTeleportCommand;
import minez.Skill.Listener.SkillTreeListener;
import minez.Skill.Listener.SkillUseListener;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Main extends JavaPlugin
{
    public static final String PLUGIN = "Sh99-MineZ";

    private List<IConfigurable> configs;

    private HashMap<String, FileConfiguration> config;

    @Override
    public void onEnable()
    {
        //Configurations
        this.configs = new ArrayList<>();
        this.configs.add(new DefaultConfiguration());
        this.configs.add(new BlockProtectionConfiguration());
        this.configs.add(new StarterKitConfiguration());
        this.configs.add(new CatchEntityConfiguration());
        this.configs.add(new EconomyConfiguration());
        this.loadConfigs();

        //Economy
        EconomyPeristanceStorage economyPeristanceStorage = new EconomyPeristanceStorage(this, this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration()));
        //Block Protection
        FileConfiguration blockProtectionConfig = this.config.getOrDefault("block-protection", new BlockProtectionConfiguration().getConfiguration());

        if((boolean) blockProtectionConfig.get("enabled")){
            //Persistance
            BlockPersistanceStorage blockPersistanceStorage = new BlockPersistanceStorage(this);
            ChunkPersistanceStorage chunkPeristanceStorage = new ChunkPersistanceStorage(this);

            //Initialiser
            BlockMetadataInitialiser blockMetadataInitialiser = new BlockMetadataInitialiser(this.config.getOrDefault(new BlockProtectionConfiguration().getName(), new BlockProtectionConfiguration().getConfiguration()), blockPersistanceStorage);
            //TODO: remove this
            try {
                blockMetadataInitialiser.initialise();
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }

            this.getServer().getPluginManager().registerEvents(new BlockBreakProtectionListener(chunkPeristanceStorage), this);
            this.getServer().getPluginManager().registerEvents(new BlockPlaceRegistrationListener(blockPersistanceStorage), this);
            this.getServer().getPluginManager().registerEvents(new BlockPlayerInteractionListener(chunkPeristanceStorage), this);

            this.getCommand("plot").setExecutor(new PlotCommand(chunkPeristanceStorage, economyPeristanceStorage));
            this.getCommand("plot").setTabCompleter(new PlotTabCompleter());
        }


        this.getCommand("bal").setExecutor(new BalanceCommand(economyPeristanceStorage, this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration())));
        this.getServer().getPluginManager().registerEvents(new EarnBlockBreakListener(economyPeristanceStorage, this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration())), this);
        this.getServer().getPluginManager().registerEvents(new EarnBlockPlaceListener(economyPeristanceStorage, this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration())), this);
        this.getServer().getPluginManager().registerEvents(new EarnKillEntityListener(economyPeristanceStorage, this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration())), this);

        //Commands
        if((boolean) this.config.get("config").get("commands.near.enabled")){
            this.getCommand("near").setExecutor(new NearCommand());
        }

        if((boolean) this.config.get("config").get("commands.home.enabled")){
            this.getCommand("home").setExecutor(new HomeCommand());
        }

        if((boolean) this.config.get("config").get("commands.rt.enabled")){
            this.getCommand("rt").setExecutor(new RandomTeleportCommand());
        }

        if((boolean) this.config.get("config").get("commands.back.enabled")){
            this.getCommand("back").setExecutor(new BackCommand(
                    this,
                    this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration()),
                    this.config.getOrDefault("config", new DefaultConfiguration().getConfiguration()),
                    economyPeristanceStorage
            ));
        }

        //Events
        //TODO: extract death chest and back into 2 listener
        if((boolean) this.config.get("config").get("features.player_death_chest.enabled")){
            this.getServer().getPluginManager().registerEvents(new PlayerDeathListener(
                    economyPeristanceStorage,
                    this.config.getOrDefault("config", new DefaultConfiguration().getConfiguration()),
                    this.config.getOrDefault("economy", new EconomyConfiguration().getConfiguration()),
                    this
                    ),this);
        }

        if((boolean) this.config.get("config").get("features.new_player_gift.enabled")){
            this.getServer().getPluginManager().registerEvents(new PlayerInitialiserListener(this.config.get("starter-kit")), this);
        }

        if((boolean) this.config.get("config").get("features.catch_entity.enabled")){
            this.getServer().getPluginManager().registerEvents(new CatchEntityListener(this.config.get("catch-entity")), this);
        }

        if((boolean) this.config.get("config").get("features.block_drop.enabled")){
            this.getServer().getPluginManager().registerEvents(new BlockDropListener(), this);
        }
        if((boolean) this.config.get("config").get("features.skill_tree.enabled")){
            this.getServer().getPluginManager().registerEvents(new SkillTreeListener(economyPeristanceStorage), this);
        }

        if((boolean) this.config.get("config").get("features.skill_use.enabled")){
            this.getServer().getPluginManager().registerEvents(new SkillUseListener(), this);
        }

        if((boolean) this.config.get("config").get("features.tag_notify_player.enabled")){
            this.getServer().getPluginManager().registerEvents(new TagNotifyListener(), this);
        }

        if((boolean) this.config.get("config").get("features.living_environment.enabled")){
            this.getServer().getPluginManager().registerEvents(new SignEntityListener(), this);
        }

        if((boolean) this.config.get("config").get("features.shared_loot.enabled")){
            this.getServer().getPluginManager().registerEvents(new BlockSharedLootListener(this), this);
        }

    }

    @Override
    public void onDisable()
    {

    }

    private void loadConfigs()
    {
        this.saveDefaultConfig();
        this.config = new HashMap<>();
        this.createCustomConfigs();
        this.saveConfig();
    }

    private void createCustomConfigs()
    {
        for (IConfigurable config:configs) {
            File customConfigFile = new File(this.getDataFolder(), config.getName() + ".yml");
            if (!customConfigFile.exists()) {
                customConfigFile.getParentFile().mkdirs();
                this.saveResource(config.getName() + ".yml", false);
            }

            FileConfiguration customConfig = config.getConfiguration();
            try {
                customConfig.load(customConfigFile);
            } catch (IOException | InvalidConfigurationException e) {
                e.printStackTrace();
            }
            this.config.put(config.getName(), customConfig);
        }

    }
}
