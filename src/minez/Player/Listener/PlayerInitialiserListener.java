package minez.Player.Listener;

import com.sun.org.apache.xerces.internal.xs.StringList;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class PlayerInitialiserListener implements Listener
{

    private FileConfiguration configuration;

    public PlayerInitialiserListener(FileConfiguration configuration)
    {
        this.configuration = configuration;
    }
        //TODO: change check to player is offline player
    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onPlayerJoin(PlayerJoinEvent event)
    {
        if(event.getPlayer().getLevel() > 0 || event.getPlayer().getExp() > 0){
            return false;
        }

        for(ItemStack curr:event.getPlayer().getInventory().getContents()){
            if(null == curr || curr.getType().equals(Material.AIR)){
                continue;
            }
            return false;
        }

        for(HashMap<String, Object> itemMap:(List<HashMap<String, Object>>) this.configuration.get("kit")){
            Material material = Material.valueOf((String) itemMap.get("material"));
            int amount = (int)itemMap.get("amount");

            ItemStack item = new ItemStack(material, amount);

            if(null != item.getItemMeta()){
                if(null != itemMap.getOrDefault("name", null)){
                    ItemMeta itemMeta = item.getItemMeta();
                    itemMeta.setDisplayName((String) itemMap.get("name"));
                    item.setItemMeta(itemMeta);
                }
            }

            if(null == itemMap.getOrDefault("enchantments", null)){
                event.getPlayer().getInventory().addItem(item);
                continue;
            }

            for(HashMap<String, Object> enchantmentMap:(List<HashMap<String, Object>>) itemMap.get("enchantments")){
                item.addEnchantment(Enchantment.getByName((String) enchantmentMap.get("enchantment")), (int) enchantmentMap.get("level"));
            }

            event.getPlayer().getInventory().addItem(item);
        }

        event.getPlayer().updateInventory();

        return true;
    }

}
