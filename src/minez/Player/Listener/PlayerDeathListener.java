package minez.Player.Listener;

import Economy.Storage.EconomyPeristanceStorage;
import minez.Main;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class PlayerDeathListener implements Listener
{
    private EconomyPeristanceStorage economyPeristanceStorage;

    private FileConfiguration defaultConfiguration;

    private FileConfiguration economyConfiguration;

    private JavaPlugin plugin;

    public PlayerDeathListener(
            EconomyPeristanceStorage economyPeristanceStorage,
            FileConfiguration defaultConfiguration,
            FileConfiguration economyConfiguration,
            JavaPlugin plugin
    ){
        this.economyPeristanceStorage = economyPeristanceStorage;
        this.defaultConfiguration = defaultConfiguration;
        this.economyConfiguration = economyConfiguration;
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onPlayerDeath(PlayerDeathEvent event)
    {
        event.getEntity().setMetadata("death-location", new FixedMetadataValue(this.plugin, event.getEntity().getLocation()));

        event.getEntity().sendMessage(
                ChatColor.YELLOW + "You can do " +
                   ChatColor.RED + "/back" +
                   ChatColor.YELLOW + " to get right back to your death location. It costs " +
                   ChatColor.RED + this.defaultConfiguration.get("commands.back.costs") + " " + this.economyConfiguration.get("currency") +
                   ChatColor.YELLOW + " to use this command."
        );

        event.setKeepInventory(true);
        event.setKeepLevel(true);

        if(event.getNewLevel() > 1){
            event.setNewLevel(event.getNewLevel()-1);
            event.setNewExp(0);
        }

        event.getEntity().getLocation().getBlock().setType(Material.CHEST);

        Chest chest = (Chest) event.getEntity().getLocation().getBlock().getState();

        for(ItemStack item:event.getEntity().getInventory().getContents()){
            if(null == item){
                continue;
            }
            chest.getInventory().addItem(item);
        }

        event.getDrops().clear();

        event.getEntity().getInventory().clear();
        event.getEntity().updateInventory();

        event.getEntity().getLocation().getBlock().getChunk().unload();
        event.getEntity().getLocation().getBlock().getChunk().load();

        if(null == event.getDeathMessage()){
            return false;
        }

        if(null == event.getEntity().getKiller()){
            event.setDeathMessage(ChatColor.RED + event.getDeathMessage());
            return true;
        }

        if(!event.getDeathMessage().contains(event.getEntity().getKiller().getName())){
            event.setDeathMessage(ChatColor.RED + event.getDeathMessage());
            return true;
        }
        event.setDeathMessage(ChatColor.RED + event.getDeathMessage().replace(event.getEntity().getKiller().getName(), ChatColor.DARK_RED + event.getEntity().getKiller().getName()));
        return true;
    }
}
