package minez.Player.Configuration;

import config.IConfigurable;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StarterKitConfiguration implements IConfigurable
{
    @Override
    public String getName() {
        return "starter-kit";
    }

    @Override
    public FileConfiguration getConfiguration() {
        FileConfiguration config = new YamlConfiguration();
        List<HashMap<String, Object>> list = new ArrayList<>();

        config.set("kit", list);

        return config;
    }

}