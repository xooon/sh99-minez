package minez;

import org.bukkit.Chunk;

import java.beans.Introspector;
import java.util.ArrayList;
import java.util.List;

public class Utils
{
    public static String decapitalizeSeperatedString(String capitalized, String seperator)
    {
        StringBuilder decapitalizedString = new StringBuilder();

        if(!capitalized.contains(seperator)){
            return decapitalizeString(capitalized);
        }

        for(String string:capitalized.split(seperator)){
            decapitalizedString.append(decapitalizeString(string)).append(" ");
        }

        return decapitalizedString.toString();
    }

    public static String decapitalizeString(String capitalized)
    {
        String capital = capitalized.substring(0, 1).toUpperCase();
        String capitalizedWithoutCapital = capitalized.substring(1).toLowerCase();

        return capital + capitalizedWithoutCapital;
    }

    public static String getChunkUuId(Chunk chunk)
    {
        return "chk_" + chunk.getX() + "_" + chunk.getZ() + "_" + chunk.getWorld().getUID().toString();
    }
}
