package minez.Skill.Listener;

import Economy.Storage.EconomyPeristanceStorage;
import minez.Skill.Gui.SkillGui;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.List;

public class SkillTreeListener implements Listener
{
    private EconomyPeristanceStorage economyPeristanceStorage;

    public SkillTreeListener(EconomyPeristanceStorage economyPeristanceStorage)
    {
        this.economyPeristanceStorage = economyPeristanceStorage;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onOpenSkillTree(PlayerInteractEvent event)
    {
        if(!event.getPlayer().getInventory().getItemInMainHand().getType().equals(Material.AIR)){
            return false;
        }

        if(null == event.getClickedBlock()){
            return false;
        }

        if(!event.getClickedBlock().getType().equals(Material.BOOKSHELF)){
            return false;
        }

        event.getPlayer().openInventory(SkillGui.createGui());

        return true;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onClickSkillOnSkillTree(InventoryClickEvent event)
    {
        if(null == event.getClickedInventory()){
            return false;
        }

        if(!Arrays.equals(event.getClickedInventory().getContents(), SkillGui.createGui().getContents())){
            return false;
        }

        if(null == event.getCurrentItem()){
            return false;
        }

        if(event.getCurrentItem().getType().equals(Material.AIR)){
            return false;
        }

        if(!(event.getWhoClicked() instanceof Player)){
            return false;
        }

        Player player = (Player) event.getWhoClicked();

        ItemStack item = event.getCurrentItem();

        if(null == item.getItemMeta()){
            return false;
        }

        ItemMeta itemMeta = item.getItemMeta();

        if(null == itemMeta.getLore()){
            return false;
        }

        List<String> lore = itemMeta.getLore();

        double costs = 0;

        for(String loreItem:lore){
            if(!loreItem.contains("€")){
                continue;
            }
            costs = Double.valueOf(loreItem.split("€ ")[1]);
        }

        if(costs <= 0){
            return false;
        }

        YamlConfiguration eco = this.economyPeristanceStorage.get(player);

        double balance = eco.getDouble("balance");

        if(balance < costs){
            event.setCancelled(true);
            player.sendMessage(ChatColor.RED + "You don't have enought € to buy this skill.");
            return false;
        }

        if(null == this.economyPeristanceStorage.add(player, -costs)){
            return false;
        }

        player.sendMessage(ChatColor.LIGHT_PURPLE + "You buyed a magic skill for " + ChatColor.GOLD + costs + " €" + ChatColor.WHITE + ".");
        player.getInventory().addItem(item);
        event.setCancelled(true);
        return true;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onDragSkillOnSkillTree(InventoryDragEvent event)
    {
        if(!Arrays.equals(event.getInventory().getContents(), SkillGui.createGui().getContents())){
            return false;
        }

        event.setCancelled(true);

        return true;
    }
}
