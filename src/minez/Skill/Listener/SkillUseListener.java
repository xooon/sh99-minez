package minez.Skill.Listener;

import minez.Skill.Item.SkillItem;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SkillUseListener implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onSkillUse(PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        PlayerInventory inventory = player.getInventory();
        ItemStack mainHandItem = inventory.getItemInMainHand();

        if(!SkillItem.isSkillItem(mainHandItem)){
            return false;
        }

        if(null == mainHandItem.getItemMeta()){
            return false;
        }

        ItemMeta itemMeta = mainHandItem.getItemMeta();

        if(null == itemMeta.getLore()){
            return false;
        }
        List<String> lore = itemMeta.getLore();

        String skillClass = lore.get(lore.size()-1);

        if(null == skillClass){
            return false;
        }

        SkillItem skill = SkillItem.getSkillByClass(skillClass);

        if(null == skill){
            return false;
        }

        float costs = 0;
        int level = 1;
        for(String loreItem:lore){
            if(loreItem.contains("LV ")){
                level = Integer.valueOf(loreItem.split("LV ")[1]);
                continue;
            }
            if(!loreItem.contains("MP")){
                continue;
            }
            costs = Float.valueOf(loreItem.split("MP ")[1])/100;
        }

        if(costs <= 0){
            return false;
        }

        if(costs > event.getPlayer().getExp()){
            if(event.getPlayer().getLevel() <= 0){
                event.setCancelled(true);
                player.sendMessage(ChatColor.RED + "You don't have enought MP/EXP to use the skill.");
                return false;
            }
            event.getPlayer().setExp((1-costs+player.getExp()) <= 0 ? 0 : 1-costs+player.getExp());
            event.getPlayer().setLevel(event.getPlayer().getLevel()-1);
        }else {
            event.getPlayer().setExp((player.getExp()-costs) <= 0 ? 0 : player.getExp()-costs);

        }

        Random r = new Random();
        int rv = r.nextInt(20)+1;

        List<String> newLore = new ArrayList();

        if(rv <= 3){
            for(String loreLv:lore){
                if(loreLv.contains("MP ")){
                    newLore.add(ChatColor.BLUE + "MP " + String.valueOf(Integer.valueOf(loreLv.split("MP ")[1])+5));
                    continue;
                }

                if(!loreLv.contains("LV ")){
                    newLore.add(loreLv);
                    continue;
                }
                 level = Integer.valueOf(loreLv.split("LV ")[1]);
                if(level < 100)
                {
                    ++level;
                }
                newLore.add(ChatColor.WHITE + "LV " + level);
            }
        }else {
            newLore = lore;
        }

        itemMeta.setLore(newLore);
        mainHandItem.setItemMeta(itemMeta);

        event.getPlayer().getInventory().setItemInMainHand(mainHandItem);

        skill.effect(player, level);

        return true;
    }
}
