package minez.Skill.Item;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class TestSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem() {
        return SkillItem.createGuiItem(Material.STONE_HOE, 1, "Test", 1, this.costs(), SkillItem.FIRE_ELEMENT, TestSkillItem.class.toString());
    }

    @Override
    public int costs() {
        return 5;
    }

    @Override
    public void effect(Player player, int level) {
        player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, 50 , 200));
    }
}
