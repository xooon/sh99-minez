package minez.Skill.Item;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class HealSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.PRISMARINE_CRYSTALS, 1, ChatColor.GRAY + "Heal", 1, this.costs(), SkillItem.ILLUSION_ELEMENT, HealSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        int amplifier = 3*level;


        if(amplifier > 255){
            amplifier = 255;
        }

        player.addPotionEffect(new PotionEffect(PotionEffectType.HEAL, amplifier*2, amplifier));
        player.addPotionEffect(new PotionEffect(PotionEffectType.ABSORPTION, amplifier*2, amplifier));
    }
}
