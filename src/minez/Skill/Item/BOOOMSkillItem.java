package minez.Skill.Item;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LightningStrike;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

public class BOOOMSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.GUNPOWDER, 1, ChatColor.RED + "BOOOM", 1, this.costs(), SkillItem.FIRE_ELEMENT, BOOOMSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        float radius = 0.1F;

        Location location = player.getEyeLocation();
        location.setDirection(location.getDirection().multiply(10));

        player.getWorld().createExplosion(location, (radius*level));

        player.setLastDamage(0);
    }
}
