package minez.Skill.Item;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class TeleportSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.FEATHER, 1, ChatColor.GRAY + "Teleport", 1, this.costs(), SkillItem.ILLUSION_ELEMENT, TeleportSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        player.setVelocity((player.getFacing().getDirection().multiply(level)));
    }
}
