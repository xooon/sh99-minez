package minez.Skill.Item;


import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Random;

public class FireballSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.FIRE_CHARGE, 1, ChatColor.RED + "Fireball", 1, this.costs(), SkillItem.FIRE_ELEMENT, FireballSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        for(int i = 0;i < level;i += 2){
            Location location = player.getLocation().clone();
            location.setY(location.getY()+1+(i*2));
            Entity fireball = player.getWorld().spawnEntity(location, EntityType.FIREBALL);
            fireball.setVelocity((fireball.getVelocity().multiply(10.0)));
        }
    }
}
