package minez.Skill.Item;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class WaterfallSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.GHAST_TEAR, 1, ChatColor.AQUA + "Waterfall", 1, this.costs(), SkillItem.WATER_ELEMENT, WaterfallSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        Location location = player.getEyeLocation();

        location.setY(location.getY()+((int)(level/2)));

        location.getBlock().setType(Material.WATER);

        player.getWorld().spawnFallingBlock(location, location.getBlock().getBlockData());
        player.teleport(location);
    }
}
