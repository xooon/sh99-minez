package minez.Skill.Item;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Random;

public class LeafcageSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.KELP, 1, ChatColor.GREEN + "Leafcage", 1, this.costs(), SkillItem.LAND_ELEMENT, LeafcageSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        Location location = player.getLocation().clone();
        location.getBlock().setType(Material.DARK_OAK_LEAVES);

        for(int y = 0;y < level;++y){
            location.setY(location.getY()+1);
            location.getBlock().setType(Material.DARK_OAK_LEAVES);
        }

        location.setY(location.getY()+1);
        player.teleport(location);
    }

}
