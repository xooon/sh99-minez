package minez.Skill.Item;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Random;

public class BlizzardSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.PRISMARINE_SHARD, 1, ChatColor.AQUA + "Blizzard", 1, this.costs(), SkillItem.WATER_ELEMENT, BlizzardSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        Location location = player.getLocation();

        Random r = new Random();

        level = level/4;

        for(int y = 0;y < level;++y){
            for(int x = 0;x < level;++x){
                for(int z = 0;z < level;++z){
                    if((r.nextInt(3)+1) > 1){
                        continue;
                    }

                    Location clone = location.clone();

                    clone.setX((location.getX()+x)-3);
                    clone.setY(location.getY()+y);
                    clone.setZ((location.getZ()+z)-3);
                    clone.getBlock().setType(Material.ICE);
                }
            }
        }

        Location playerSafeZone = player.getLocation().clone();

        playerSafeZone.getBlock().setType(Material.AIR);

        playerSafeZone.setY(playerSafeZone.getY()+1);
        playerSafeZone.getBlock().setType(Material.AIR);
    }
}
