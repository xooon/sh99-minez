package minez.Skill.Item;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class WeedSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.GRASS, 1, ChatColor.GRAY + "Weed", 1, this.costs(), ILLUSION_ELEMENT, WeedSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        Random r = new Random();

        List<PotionEffect> effects = new ArrayList<>();

        effects.add(new PotionEffect(PotionEffectType.SPEED, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.SLOW, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.FAST_DIGGING, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.SLOW_DIGGING, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.HARM, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.JUMP, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.CONFUSION, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.HEAL, (level)*5, 200));
        effects.add(new PotionEffect(PotionEffectType.REGENERATION, (level)*5, 100));
        effects.add(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.WATER_BREATHING, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.INVISIBILITY, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.BLINDNESS, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.NIGHT_VISION, (level)*5, 100));
        effects.add(new PotionEffect(PotionEffectType.BLINDNESS, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.HUNGER, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.WEAKNESS, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.POISON, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.WITHER, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.HEALTH_BOOST, (level)*5, 100));
        effects.add(new PotionEffect(PotionEffectType.ABSORPTION, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.SATURATION, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.GLOWING, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.LEVITATION, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.LUCK, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.UNLUCK, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.SLOW_FALLING, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.CONDUIT_POWER, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.DOLPHINS_GRACE, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.BAD_OMEN, (level)*5, 50));
        effects.add(new PotionEffect(PotionEffectType.HERO_OF_THE_VILLAGE, (level)*5, 50));

        if(level < effects.size()){
            for(int i = 0;i < level;++i){
                player.addPotionEffect(effects.get(i));
            }
        }else {
            for (PotionEffect effect:effects){
                player.addPotionEffect(effect);
            }
        }

    }
}
