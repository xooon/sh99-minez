package minez.Skill.Item;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface SkillItem
{
    public static final String FIRE_ELEMENT = ChatColor.RED + "Fire";
    public static final String WATER_ELEMENT = ChatColor.AQUA + "Water";
    public static final String LAND_ELEMENT = ChatColor.GREEN + "Land";
    public static final String ELECTRO_ELEMENT = ChatColor.YELLOW + "Electro";
    public static final String ILLUSION_ELEMENT = ChatColor.BLACK + "Illusion";

    public ItemStack createItem();

    public int costs();

    public void effect(Player player, int level);

    public static ItemStack createGuiItem(Material material, int amount, String name, int level, int costs, String element, String skillClass)
    {
        ItemStack item = new ItemStack(material, amount);

        if(null == item.getItemMeta()){
            return item;
        }

        ItemMeta itemMeta = item.getItemMeta();

        if(!(null == name)){
            itemMeta.setDisplayName(name);
        }

        List<String> lore = new ArrayList<>();

        lore.add(ChatColor.MAGIC + SkillItem.class.toString());

        if(!(null == element)){
            lore.add(element);
        }

        int price = 100;

        lore.add(ChatColor.BLUE + "MP " + costs);
        lore.add(ChatColor.WHITE + "LV " + level);
        lore.add(ChatColor.GOLD + "€ " + price);
        lore.add(ChatColor.MAGIC + skillClass);

        itemMeta.setLore(lore);

        item.setItemMeta(itemMeta);


        return item;
    }

    public static Boolean isSkillItem(ItemStack item)
    {
        if(null == item.getItemMeta()){
            return false;
        }

        if(null == item.getItemMeta().getLore()){
            return false;
        }

        for (String lore:item.getItemMeta().getLore()){
            if(!lore.equals(ChatColor.MAGIC + SkillItem.class.toString())){
                continue;
            }
            return true;
        }

        return false;
    }

    public static SkillItem getSkillByClass(String skillClass)
    {
        HashMap<String, SkillItem> map = new HashMap<>();

        map.put(ChatColor.MAGIC + FireballSkillItem.class.toString(), new FireballSkillItem());
        map.put(ChatColor.MAGIC + HealSkillItem.class.toString(), new HealSkillItem());
        map.put(ChatColor.MAGIC + TeleportSkillItem.class.toString(), new TeleportSkillItem());
        map.put(ChatColor.MAGIC + WaterfallSkillItem.class.toString(), new WaterfallSkillItem());
        map.put(ChatColor.MAGIC + ThunderboltSkillItem.class.toString(), new ThunderboltSkillItem());
        map.put(ChatColor.MAGIC + BOOOMSkillItem.class.toString(), new BOOOMSkillItem());
        map.put(ChatColor.MAGIC + BlizzardSkillItem.class.toString(), new BlizzardSkillItem());
        map.put(ChatColor.MAGIC + LeafcageSkillItem.class.toString(), new LeafcageSkillItem());

        if(!map.containsKey(skillClass)){
            return null;
        }

        return map.get(skillClass);
    }
}
