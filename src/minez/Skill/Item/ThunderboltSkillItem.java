package minez.Skill.Item;


import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import java.util.Random;

public class ThunderboltSkillItem implements SkillItem
{
    @Override
    public ItemStack createItem()
    {
        return SkillItem.createGuiItem(Material.BLAZE_ROD, 1, ChatColor.YELLOW + "Thunderbolt", 1, this.costs(), SkillItem.ELECTRO_ELEMENT, ThunderboltSkillItem.class.toString());
    }

    @Override
    public int costs()
    {
        return 5;
    }

    @Override
    public void effect(Player player, int level)
    {
        int i = 0;

       for(Entity entity:player.getNearbyEntities(
               player.getLocation().getX(),
               player.getLocation().getY(),
               player.getLocation().getZ()
       )){
           if(!(entity instanceof Monster)){
               continue;
           }

           if(i > level){
               return;
           }

           LightningStrike ls = player.getWorld().strikeLightning(entity.getLocation());
           ls.setFireTicks(0);
           ++i;
       }
    }
}
