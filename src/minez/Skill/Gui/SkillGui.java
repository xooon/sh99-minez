package minez.Skill.Gui;

import minez.Skill.Item.*;
import org.bukkit.Bukkit;
import org.bukkit.inventory.Inventory;


public class SkillGui
{

    public static Inventory createGui()
    {
        Inventory inventory = Bukkit.createInventory(null, 45, "Skill Tree");

        //Row 1
        inventory.setItem(0, (new FireballSkillItem()).createItem());
        inventory.setItem(1, (new BOOOMSkillItem().createItem()));

        //Row 2
        inventory.setItem(9, (new HealSkillItem().createItem()));
        inventory.setItem(10, (new TeleportSkillItem().createItem()));

        //Row 3
        inventory.setItem(18, (new WaterfallSkillItem().createItem()));
        inventory.setItem(19, (new BlizzardSkillItem().createItem()));

        //Row 4
        inventory.setItem(27, (new ThunderboltSkillItem().createItem()));

        //Row 5
        inventory.setItem(36, (new LeafcageSkillItem().createItem()));

        return inventory;
    }
}
