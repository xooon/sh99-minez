package minez.Notify.Listener;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.ArrayList;
import java.util.List;


public class TagNotifyListener implements Listener
{

    @EventHandler(priority = EventPriority.LOWEST)
    public Boolean onPlayerNamed(AsyncPlayerChatEvent event)
    {
        String message = event.getMessage();


        if(message.contains("everyone")) {
            message = message.replace("@everyone", ChatColor.YELLOW + "@everyone" + ChatColor.WHITE);
            for (Player onlinePlayer:Bukkit.getOnlinePlayers()){
                onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1.0F, 1.0F);
            }
        }

        for (Player onlinePlayer:Bukkit.getOnlinePlayers()){
            if(!message.contains(onlinePlayer.getName())){
                continue;
            }
            message = message.replace(onlinePlayer.getName(), ChatColor.YELLOW + "@" + onlinePlayer.getName() + ChatColor.WHITE);
            onlinePlayer.playSound(onlinePlayer.getLocation(), Sound.BLOCK_ANVIL_PLACE, 1.0F, 1.0F);
        }

        event.setMessage(message);

        return true;
    }

}

