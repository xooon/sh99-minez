package minez.Home.Command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;

public class HomeCommand implements CommandExecutor
{

    private static final String HOME_ITEM_NAME = ChatColor.DARK_RED + "Home";

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)){
            return false;
        }

        Player player = (Player) sender;

        ItemStack firstHandItem = player.getInventory().getItemInMainHand();
        ItemMeta firstHandItemMeta = firstHandItem.getItemMeta();

        if(firstHandItem.getType().equals(Material.AIR)){
            ItemStack compass = new ItemStack(Material.COMPASS, 1);
            ItemMeta compassMeta = compass.getItemMeta();

            if(null == compassMeta){
                return false;
            }

            List<String> lore = new ArrayList<>();
            lore.add("" + player.getLocation().getX());
            lore.add("" + player.getLocation().getY());
            lore.add("" + player.getLocation().getZ());

            compassMeta.setLore(lore);

            compassMeta.setDisplayName(HOME_ITEM_NAME);
            compass.setItemMeta(compassMeta);

            player.getInventory().setItemInMainHand(compass);
            player.sendMessage(ChatColor.DARK_GREEN + "You home has been stored to your inventory. Just use /home while holding it in your hands to get back right here.");

            return true;
        }

        if(null == firstHandItemMeta){
            return false;
        }


        if(!firstHandItemMeta.hasDisplayName()){
            return false;
        }

        if(!firstHandItemMeta.getDisplayName().equals(HOME_ITEM_NAME) && !firstHandItem.getType().equals(Material.AIR)){
            return false;
        }


        List<String> lore = firstHandItemMeta.getLore();

        if(null == lore ||lore.size() < 3){
            return false;
        }

        double x = Double.parseDouble(lore.get(0));
        double y = Double.parseDouble(lore.get(1));
        double z = Double.parseDouble(lore.get(2));

        Location location = player.getLocation().clone();
        location.setX(x);
        location.setY(y);
        location.setZ(z);

        player.teleport(location);
        player.sendMessage(ChatColor.GREEN + "You got back to your home.");

        return true;
    }
}
