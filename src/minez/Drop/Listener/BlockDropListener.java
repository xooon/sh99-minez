package minez.Drop.Listener;

import minez.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.ExperienceOrb;
import org.bukkit.entity.Item;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class BlockDropListener implements Listener
{
    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onPickaxeBreaksBlock(BlockBreakEvent event)
    {
        if(event.isCancelled()){
            return false;
        }

        Random r = new Random();

        if((r.nextInt(100)+1) > 5){
            return false;
        }

        List<ItemStack> rewards = this.getRewardItemList();
        int rewardsSize = rewards.size();

        ItemStack reward = rewards.get(r.nextInt(rewardsSize));

        Item rewardItem = event.getPlayer().getWorld().dropItem(event.getBlock().getLocation(), reward);
        rewardItem.setCustomNameVisible(true);
        rewardItem.setCustomName(ChatColor.GREEN + "LuckyDrop" + ChatColor.WHITE + Utils.decapitalizeSeperatedString(rewardItem.getItemStack().getType().toString(), "_"));

        Item expRewardItem = event.getPlayer().getWorld().dropItem(event.getBlock().getLocation(), new ItemStack(Material.EXPERIENCE_BOTTLE, 1));
        expRewardItem.setCustomNameVisible(true);
        expRewardItem.setCustomName(ChatColor.GREEN + "LuckyDrop" + ChatColor.WHITE + Utils.decapitalizeSeperatedString(expRewardItem.getItemStack().getType().toString(), "_"));

        ExperienceOrb orb = (ExperienceOrb) event.getPlayer().getWorld().spawnEntity(event.getPlayer().getLocation(), EntityType.EXPERIENCE_ORB);
        orb.setExperience(1);

        return true;
    }

    private List<ItemStack> getRewardItemList()
    {
        List<ItemStack> rewards = new ArrayList<>();

        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));
        rewards.add(new ItemStack(Material.STONE));

        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));
        rewards.add(new ItemStack(Material.GRASS_BLOCK));

        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));
        rewards.add(new ItemStack(Material.GLOWSTONE_DUST));

        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));
        rewards.add(new ItemStack(Material.COAL));

        rewards.add(new ItemStack(Material.IRON_INGOT));
        rewards.add(new ItemStack(Material.IRON_INGOT));
        rewards.add(new ItemStack(Material.IRON_INGOT));
        rewards.add(new ItemStack(Material.IRON_INGOT));
        rewards.add(new ItemStack(Material.IRON_INGOT));

        rewards.add(new ItemStack(Material.GOLD_INGOT));
        rewards.add(new ItemStack(Material.GOLD_INGOT));
        rewards.add(new ItemStack(Material.GOLD_INGOT));
        rewards.add(new ItemStack(Material.GOLD_INGOT));
        rewards.add(new ItemStack(Material.GOLD_INGOT));

        rewards.add(new ItemStack(Material.EMERALD));
        rewards.add(new ItemStack(Material.EMERALD));

        rewards.add(new ItemStack(Material.DIAMOND));





        return rewards;
    }
}
