package minez.Drop.Listener;

import minez.Skill.Item.SkillItem;
import minez.Utils;
import net.minecraft.server.v1_14_R1.ItemCompass;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityPickupItemEvent;
import org.bukkit.event.entity.ItemSpawnEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

import java.util.Arrays;
import java.util.List;

public class BlockSharedLootListener implements Listener
{
    private Plugin plugin;

    public BlockSharedLootListener(Plugin plugin)
    {
        this.plugin = plugin;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemDrop(ItemSpawnEvent event)
    {
        if(event.getEntity().getItemStack().getType().equals(Material.CHORUS_FRUIT)){
            return;
        }

        if(isControlled(event.getEntity())){
            return;
        }

        if(event.getEntity().getItemStack().getType().equals(Material.COMPASS)){
            return;
        }

        if(SkillItem.isSkillItem(event.getEntity().getItemStack())){
            return;
        }

        String name;

        Item entity = event.getEntity();

        Material material = entity.getItemStack().getType();

        entity.setCustomNameVisible(true);

        if(!entity.isCustomNameVisible()){
            name = entity.getCustomName();
        }else {
            name = Utils.decapitalizeSeperatedString(material.toString(), "_");
        }

        if(name.contains(ChatColor.AQUA + "#") || name.contains("LuckyDrop")){
            event.getEntity().getServer().broadcastMessage("TEST");
            return;
        }


        List<Entity> nearEntities = entity.getNearbyEntities(
                entity.getLocation().getX(),
                entity.getLocation().getY(),
                entity.getLocation().getZ()
        );

        ItemStack item = entity.getItemStack();
        ItemMeta itemMeta = item.getItemMeta();
        itemMeta.setLore(Arrays.asList("controlled"));


        for(Entity nearEntity:nearEntities){
            if(!(nearEntity instanceof Player)){
                continue;
            }

            Location nearEntityLocation = nearEntity.getLocation().clone();
            Location itemLocation = entity.getLocation();

            double distance = nearEntityLocation.distance(itemLocation);

            if(distance > 25){
                continue;
            }

            itemMeta.setDisplayName(ChatColor.MAGIC + nearEntity.getName());
            item.setItemMeta(itemMeta);

            String customPlayerItemName = ChatColor.MAGIC + "" + ChatColor.AQUA + "#" + ChatColor.RED + " " + nearEntity.getName() + ChatColor.WHITE +  " " + name;
            Item droppedItem = entity.getWorld().dropItem(nearEntity.getLocation(), item);
            droppedItem.setCustomNameVisible(true);
            droppedItem.setCustomName(customPlayerItemName);
            droppedItem.setPickupDelay(10);
        }

        entity.remove();
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onItemPickup(EntityPickupItemEvent event)
    {
        if(!(event.getEntity() instanceof Player)){
            return;
        }

        if(event.getItem().getItemStack().getType().equals(Material.COMPASS)){
            return;
        }

        if(event.getItem().getItemStack().getType().equals(Material.CHORUS_FRUIT)){
            return;
        }

        if(SkillItem.isSkillItem(event.getItem().getItemStack())){
            return;
        }


        if(!isControlled(event.getItem())){
            return;
        }

        Player player = (Player) event.getEntity();
        Item pickedItem = event.getItem();

        if(!pickedItem.isCustomNameVisible()){
            return;
        }

        String customName = pickedItem.getCustomName();

        if(customName.contains(ChatColor.AQUA + "Item"))
        {
            return;
        }

        //TODO: do this some better way...
        if(customName.contains(player.getName())){
            ItemStack itemStack = event.getItem().getItemStack();
            if(null == itemStack.getItemMeta()){
                return;
            }
            ItemMeta meta = itemStack.getItemMeta();
            if(!meta.getDisplayName().contains(ChatColor.MAGIC + event.getEntity().getName())){
                event.setCancelled(true);
                return;
            }
            ItemStack copyStack = new ItemStack(itemStack.getType(), 1);
            meta.setDisplayName(meta.getDisplayName().replace(ChatColor.MAGIC + event.getEntity().getName(), ""));
            itemStack.setItemMeta(meta);
            event.getItem().setItemStack(itemStack);
            return;
        }

        event.setCancelled(true);
    }


    private static Boolean isControlled(Item item)
    {
        ItemStack itemStack = item.getItemStack();

        if(null == itemStack.getItemMeta()){
            return false;
        }

        List<String> lore = itemStack.getItemMeta().getLore();

        if(null == lore){
            return false;
        }

        if(lore.size() <= 0){
            return false;
        }

        String firstArg = lore.get(0);

        if(!firstArg.equals("controlled")){
            return false;
        }

        return true;
    }
}
