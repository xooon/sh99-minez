package minez.Back;

import Economy.Storage.EconomyPeristanceStorage;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;

public class BackCommand implements CommandExecutor
{

    private JavaPlugin plugin;
    private FileConfiguration economyConfiguration;
    private FileConfiguration defaultConfiguration;
    private EconomyPeristanceStorage economyPeristanceStorage;

    public BackCommand(
            JavaPlugin plugin,
            FileConfiguration economyConfiguration,
            FileConfiguration defaultConfiguration,
            EconomyPeristanceStorage economyPeristanceStorage
    )
    {
        this.plugin = plugin;
        this.economyConfiguration = economyConfiguration;
        this.defaultConfiguration = defaultConfiguration;
        this.economyPeristanceStorage = economyPeristanceStorage;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)){
            return false;
        }

        Player player = (Player) sender;

        if(!player.hasMetadata("death-location")){
            player.sendMessage(ChatColor.RED + "You were not died in the current session.");
            return true;
        }

        List<MetadataValue> metas = player.getMetadata("death-location");

        for(MetadataValue meta:metas){
            if(null == meta.value()){
                continue;
            }

            if(!(meta.value() instanceof Location)){
                continue;
            }

            double costs = (double) this.defaultConfiguration.get("commands.back.costs");

            if(costs > 0){
                costs = (costs-(costs+costs));
            }

            YamlConfiguration config = this.economyPeristanceStorage.add(player, costs);

            if(null == config){
                player.sendMessage(ChatColor.RED + "Can not perform this command yet.");
                return true;
            }

            player.teleport((Location) meta.value());
            player.sendMessage(
                        ChatColor.GREEN + "You have been teleported to your " +
                          ChatColor.RED + "death location" +
                          ChatColor.YELLOW + " for " +
                          ChatColor.RED + costs + this.economyConfiguration.get("currency") +
                          ChatColor.GREEN + "."
            );
            player.removeMetadata("death-location", this.plugin);
            break;
        }
        return true;
    }
}
