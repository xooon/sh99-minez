package minez.Catch.Configuration;

import config.IConfigurable;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class CatchEntityConfiguration implements IConfigurable
{
        @Override
        public String getName() {
            return "catch-entity";
        }

        @Override
        public FileConfiguration getConfiguration() {
            FileConfiguration config = new YamlConfiguration();
            List<HashMap<String, Object>> list = new ArrayList<>();


            config.set("entities", list);

            return config;
        }

}