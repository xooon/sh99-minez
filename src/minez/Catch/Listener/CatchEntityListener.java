package minez.Catch.Listener;


import com.sun.org.apache.xerces.internal.xs.datatypes.ObjectList;
import javafx.scene.transform.MatrixType;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.projectiles.ProjectileSource;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;

public class CatchEntityListener implements Listener {

    private static final int MAX_DROP_CHANCE = 100;

    private FileConfiguration configuration;

    public CatchEntityListener(FileConfiguration configuration)
    {
        this.configuration = configuration;
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    public Boolean onProjectileHit(EntityDamageByEntityEvent event)
    {
        if(event.isCancelled()){
            return false;
        }

        //if damager is not an egg
        if(!(event.getDamager() instanceof Snowball)){
            return false;
        }

        Snowball snowball = (Snowball) event.getDamager();

        if(!(snowball.getShooter() instanceof Player)){
            return false;
        }

        Player shooter = (Player) snowball.getShooter();

        if(null == shooter){
            return false;
        }

        //if entity is a player
        if(event.getEntity() instanceof Player){
            return false;
        }

        //if entity is not living anymore
        if(!(event.getEntity() instanceof LivingEntity)){
            return false;
        }

        event.setCancelled(true);

        EntityType entityType = event.getEntityType();
        Material material = null;
        int dropChance = 0;

        for (Object entityConfig:(List<Object>) this.configuration.get("entities"))
        {
            LinkedHashMap<String, Object> map = (LinkedHashMap<String, Object>) entityConfig;

            if(!entityType.equals(EntityType.valueOf((String) map.get("entity")))){
                continue;
            }

            material = Material.valueOf((String) map.get("egg"));
            dropChance = (int) map.get("chance");
        }

        if(null == material){
            return false;
        }

        World world = event.getEntity().getWorld();
        Location location = event.getEntity().getLocation();

        ItemStack eggDrop = new ItemStack(material, 1);

        Random r = new Random();

        int rInt = r.nextInt(100)+1;

        if(rInt <= 0){
            rInt = 1;
        }
        Entity eventEntity = event.getEntity();
        String s = "a";
        //TODO: do it way better than this...
        if(
                eventEntity.getName().substring(0, 1).equals("a") ||
                        eventEntity.getName().substring(0, 1).equals("e") ||
                        eventEntity.getName().substring(0, 1).equals("i") ||
                        eventEntity.getName().substring(0, 1).equals("o") ||
                        eventEntity.getName().substring(0, 1).equals("u")
        ){
            s = "an";
        }
        if(rInt > dropChance){
            shooter.sendMessage(ChatColor.RED + "Tried to catch " + s + " " + ChatColor.WHITE + eventEntity.getName() + ChatColor.RED + " but it escaped.");
            return false;
        }

        event.getEntity().remove();
        world.dropItem(location, eggDrop);

        shooter.sendMessage(ChatColor.GREEN + "Player " + ChatColor.WHITE + shooter.getDisplayName() + " catched " + s + ChatColor.YELLOW + " " + eventEntity.getName() + ChatColor.GREEN + ".");

        if(eventEntity.getType().equals(EntityType.ENDERMAN)){
            shooter.getServer().broadcastMessage(ChatColor.GOLD + "Player " + ChatColor.WHITE + shooter.getDisplayName() + " catched " + s + ChatColor.MAGIC + " " + eventEntity.getName() + ChatColor.GOLD + ".");
        }
        return true;
    }

}
