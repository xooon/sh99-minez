package minez.RandomTeleport.Command;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class RandomTeleportCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if(!(sender instanceof Player)){
            return false;
        }

        Random r = new Random();
        Random b = new Random();

        int randX = r.nextInt(5000) + 1;
        int randZ = r.nextInt(5000) + 1;

        if(b.nextBoolean()) {
            randX = randX-(2*randX);
        }

        if(b.nextBoolean()) {
            randZ = randZ-(2*randZ);
        }

        Location location = new Location(((Player) sender).getWorld(), (double)randX, (double)0, (double)randZ);

        for(int i = 0;i <= 150;++i){
            location.setY(i);
            sender.sendMessage(ChatColor.YELLOW + "Calculate block...");
            if(!location.getBlock().getType().equals(Material.AIR)){
                sender.sendMessage(ChatColor.RED + "Block is not valid.");
                continue;
            }

            Location belowLocation = location.clone();
            belowLocation.setY(belowLocation.getY()-1);

            if(belowLocation.getBlock().getType().equals(Material.LAVA)){
                location.setY(location.getY()+3);
                sender.sendMessage(ChatColor.RED + "Block is not valid.");
                continue;
            }

            Location upperLocation = location.clone();
            upperLocation.setY(upperLocation.getY()+1);

            if(!upperLocation.getBlock().getType().equals(Material.AIR)){
                sender.sendMessage(ChatColor.RED + "Block is not valid.");
                continue;
            }

            sender.sendMessage(ChatColor.GREEN + "Block is valid, teleport now...");
            sender.sendMessage(ChatColor.GOLD + location.getBlock().getType().toString());
            break;
        }

        ((Player) sender).teleport(location);

        sender.sendMessage(
                ChatColor.WHITE + "Randomlocation: " +
                ChatColor.GREEN + "X: " +
                ChatColor.WHITE + randX +
                ChatColor.GREEN + "Y: " +
                ChatColor.WHITE + location.getY() +
                ChatColor.GREEN + "Z: " +
                ChatColor.WHITE + randZ + "."
        );

        return true;
    }
}
