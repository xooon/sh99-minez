package minez.Near.Command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import java.util.Random;

public class NearCommand implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] strings) {
        if(!(sender instanceof Player)){
            return false;
        }


        if(strings.length <= 0){
            sender.sendMessage(ChatColor.YELLOW + "You have to tell a target name like '/near Playername'.");
            return false;
        }

        String targetPlayerName = strings[0];

        try {
            Player targetPlayer = Bukkit.getPlayer(targetPlayerName);

            Random r = new Random();
            Random b = new Random();

            int randX = (r.nextInt(100)+1)+50;
            int randZ = (r.nextInt(100)+1)+50;

            if(b.nextBoolean()) {
                randX = randX-(2*randX);
            }

            if(b.nextBoolean()) {
                randZ = randZ-(2*randZ);
            }

            double targetX = targetPlayer.getLocation().getX() + (double) randX;
            double targetY = 120;
            double targetZ = targetPlayer.getLocation().getZ() + (double) randZ;

            Location location = new Location(targetPlayer.getWorld(), targetX, targetY, targetZ);


            for(int i = 0;i <= 150;++i){
                location.setY(i);
                sender.sendMessage(ChatColor.YELLOW + "Calculate block...");
                if(!location.getBlock().getType().equals(Material.AIR)){
                    sender.sendMessage(ChatColor.RED + "Block is not valid.");
                    continue;
                }

                Location belowLocation = location.clone();
                belowLocation.setY(belowLocation.getY()-1);

                if(belowLocation.getBlock().getType().equals(Material.LAVA)){
                    location.setY(location.getY()+3);
                    sender.sendMessage(ChatColor.RED + "Block is not valid.");
                    continue;
                }

                Location upperLocation = location.clone();
                upperLocation.setY(upperLocation.getY()+1);

                if(!upperLocation.getBlock().getType().equals(Material.AIR)){
                    sender.sendMessage(ChatColor.RED + "Block is not valid.");
                    continue;
                }

                sender.sendMessage(ChatColor.GREEN + "Block is valid, teleport now...");
                sender.sendMessage(ChatColor.GOLD + location.getBlock().getType().toString());
                break;
            }

            ((Player) sender).teleport(location);
            targetPlayer.sendMessage(ChatColor.RED + "Pay attention! An enemy has been teleported to your current area.");
            sender.sendMessage(ChatColor.GREEN + "Your target '" + targetPlayerName + "' is very close to you...");
            return true;
        }catch (Exception e){
            sender.sendMessage(ChatColor.YELLOW + "Target player is not online or does not exist.");
            return false;
        }
    }
}
