package LivingEnvironment.Listener;

import Block.Storage.BlockPersistanceStorage;
import com.mysql.jdbc.Util;
import minez.Utils;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;

import javax.print.DocFlavor;
import java.util.Random;

public class SignEntityListener implements Listener
{
    //TODO: sit on chairs while set player on ride mode when interact with a block (not lava etc)
    //TODO: real age
    //TODO: illness
    //TODO: feelings
    //TODO: hungry/thursty (also for players) more needings
    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onEntitySpawn(CreatureSpawnEvent event)
    {
        if(!(event.getEntity() instanceof Ageable)){
            return false;
        }

        ((Ageable) event.getEntity()).setBaby();
        ((Ageable) event.getEntity()).setBreed(false);
        ((Ageable) event.getEntity()).setAge(-1000);
        return false;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public void onEntityDeath(EntityDeathEvent event)
    {
        if(!(event.getEntity() instanceof Animals))
        {
            return;
        }

        Location location = event.getEntity().getLocation();
        location.setY(location.getY()-1);



        if(!BlockPersistanceStorage.isProtected(location.getBlock())){
            return;
        }

        Player player = BlockPersistanceStorage.getPlayer(location.getBlock());

        if(null == player){
            return;
        }

        String materialName = event.getEntity().getType().toString() + "_SPAWN_EGG";

        Material spawneggMaterial = Material.valueOf(materialName);

        player.getWorld().dropItem(player.getLocation(), new ItemStack(spawneggMaterial, 1));
    }

    @EventHandler(priority = EventPriority.NORMAL)
    public Boolean onEntityGrow(PlayerMoveEvent event)
    {
        for(Entity entity:event.getPlayer().getNearbyEntities(
            event.getPlayer().getLocation().getX(),
            event.getPlayer().getLocation().getY(),
            event.getPlayer().getLocation().getZ()
        )){
            if(!(entity instanceof Creature) && !(entity instanceof Item)){
                continue;
            }

            Location entityLocation = entity.getLocation();
            entityLocation.setY(entityLocation.getY()-1);
            Block entityBlock = entityLocation.getBlock();

            if(!BlockPersistanceStorage.isProtected(entityBlock)){

                if(entity.isCustomNameVisible()){
                    continue;
                }

                String prefix = ChatColor.GRAY + "Living " + ChatColor.WHITE;
                String name = entity.getType().toString();

                if(entity instanceof Monster){
                    prefix = ChatColor.RED + "Monster " + ChatColor.WHITE;
                }

                if(entity instanceof Ageable){
                    prefix = ChatColor.GREEN + "Living " + ChatColor.WHITE;
                }

                if(entity instanceof Item){
                    Item item = (Item) entity;
                    prefix = ChatColor.AQUA + "Item " + ChatColor.WHITE;
                    name = item.getItemStack().getType().toString();
                }

                entity.setCustomNameVisible(true);
                entity.setCustomName(prefix + Utils.decapitalizeSeperatedString(name, "_"));
                continue;
            }

            if(!(entity instanceof Ageable)){
                continue;
            }

            Ageable ageable = (Ageable) entity;
            int age = ageable.getAge();

            int convertedAge = 0;

            Random random = new Random();
            ageable.setCustomNameVisible(true);
            if(random.nextInt(100)+1 < 10){
                ageable.setAge(age+1);
            }
            if(age < 0){
                ageable.setCustomName(
                        ChatColor.GREEN +
                        "Baby " +
                        ChatColor.WHITE +
                        Utils.decapitalizeSeperatedString(ageable.getType().toString(), "_")
                );
                ageable.setBreed(false);
            }else if(age <= 20000){
                ageable.setCustomName(
                        ChatColor.YELLOW +
                        "Adult " +
                        ChatColor.WHITE +
                        Utils.decapitalizeSeperatedString(ageable.getType().toString(), "_")
                );
                ageable.setBreed(true);
            }else {
                ageable.setCustomName(
                        ChatColor.RED +
                        "Mature " +
                        ChatColor.WHITE +
                        Utils.decapitalizeSeperatedString(ageable.getType().toString(), "_")
                );
                ageable.setBreed(false);

                if(random.nextInt(100)+1 < 10){
                    ageable.remove();
                    Material material = Material.valueOf(ageable.getType().toString()+"_SPAWN_EGG");
                    ItemStack item = new ItemStack(material, 1);
                    Player blockPlayer = BlockPersistanceStorage.getPlayer(entityBlock);
                    if (null == blockPlayer) {
                        continue;
                    }
                    blockPlayer.getWorld().dropItem(blockPlayer.getLocation(), item);
                    blockPlayer.sendMessage(ChatColor.RED + "Your " + entity.getName() + " died on the natural way. You earned a spawn egg from it.");
                }
            }

        }

        return true;
    }

}
