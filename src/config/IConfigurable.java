package config;

import org.bukkit.configuration.file.FileConfiguration;

public interface IConfigurable
{
    public String getName();
    public FileConfiguration getConfiguration();
}
