package config;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class DefaultConfiguration implements IConfigurable
{
        @Override
        public String getName() {
            return "config";
        }

        @Override
        public FileConfiguration getConfiguration() {
            FileConfiguration config = new YamlConfiguration();

            config.set("commands.near.enabled", true);
            config.set("commands.home.enabled", true);
            config.set("commands.rt.enabled", true);
            config.set("commands.back.enabled", true);
            config.set("commands.back.costs", 10.00);

            config.set("features.player_death_chest.enabled", true);
            config.set("features.new_player_gift.enabled", true);
            config.set("features.catch_entity.enabled", true);
            config.set("features.block_drop.enabled", true);
            config.set("features.skill_tree.enabled", true);
            config.set("features.skill_use.enabled", true);
            config.set("features.tag_notify_player.enabled", true);
            config.set("features.living_environment.enabled", true);
            config.set("features.economy.enabled", true);
            config.set("features.shared_loot.enabled", true);


            return config;
        }

}