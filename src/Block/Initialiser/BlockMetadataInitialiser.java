package Block.Initialiser;

import Block.Dto.BlockDto;
import Block.Storage.BlockPersistanceStorage;
import minez.Main;
import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.Plugin;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class BlockMetadataInitialiser
{

    private FileConfiguration configuration;

    private BlockPersistanceStorage blockPersistanceStorage;

    public BlockMetadataInitialiser(FileConfiguration configuration, BlockPersistanceStorage blockPersistanceStorage)
    {
        this.configuration = configuration;
        this.blockPersistanceStorage = blockPersistanceStorage;
    }

    public void initialise() throws IOException, InvalidConfigurationException {
        List worlds = (List) this.configuration.get("persisted-worlds");

        Plugin plugin = Bukkit.getPluginManager().getPlugin(Main.PLUGIN);

        if (null == plugin){
            return;
        }

        if(null == worlds){
            return;
        }

        if(worlds.size() <= 0){
            return;
        }

        for(Object worldId:worlds){
            if(!(worldId instanceof String)){
                continue;
            }

            World world = Bukkit.getWorld((String) worldId);

            if(null == world){
                continue;
            }

            Location location = world.getSpawnLocation();

            for(BlockDto blockDto:this.blockPersistanceStorage.getBlocks(world)){
                location.setX(blockDto.getX());
                location.setY(blockDto.getY());
                location.setZ(blockDto.getZ());

                Block block = location.getBlock();

                MetadataValue metadataValue = new FixedMetadataValue(plugin, blockDto.getPlayer());

                block.setMetadata("player", metadataValue);
            }
        }
    }
}
