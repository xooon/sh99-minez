package Block.Dto;

public class BlockDto
{
    private double x;

    private double y;

    private double z;

    private String player;

    public BlockDto(double x, double y, double z, String player)
    {
        this.x = x;
        this.y = y;
        this.z = z;
        this.player = player;
    }

    public double getX()
    {
        return x;
    }

    public double getY()
    {
        return y;
    }

    public double getZ()
    {
        return z;
    }

    public String getPlayer()
    {
        return player;
    }
}
