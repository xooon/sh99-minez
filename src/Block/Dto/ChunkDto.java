package Block.Dto;

import com.sun.istack.internal.NotNull;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ChunkDto
{
    private String region;

    private String world;

    private int x;

    private int z;

    private String player;

    private List<String> members;

    public ChunkDto(
            @NotNull int x,
            @NotNull int z,
            @NotNull String player,
            @NotNull String world,
            @Nullable String region
    ) {
        this.x = x;
        this.z = z;
        this.player = player;
        this.world = world;
        this.region = region;
        this.members = new ArrayList<>();
    }

    public String getRegion() {
        return region;
    }

    public String getPlayer() {
        return player;
    }

    public String getWorld() {
        return world;
    }

    public int getX() {
        return x;
    }

    public int getZ() {
        return z;
    }

    public List<String> getMembers() {
        return members;
    }

    public void addMember(String player)
    {
        if(this.members.contains(player)){
            return;
        }
        this.members.add(player);
    }
}

