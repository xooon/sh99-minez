package Block.TabCompleter;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class PlotTabCompleter implements TabCompleter
{
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String s, String[] args) {
        if(!(sender instanceof Player)){
            return null;
        }

        Player player = (Player) sender;

        List<String> list = new ArrayList<>();

        if(args.length <= 1){
            list.add("buy");
            list.add("add");
            list.add("region");
        }

        if(args.length <= 2){
            switch (args[0]){
                case "add":
                    for (Player entity: Bukkit.getOnlinePlayers()) {
                        list.add(entity.getName());
                    }
            }
        }


        return list;
    }
}
