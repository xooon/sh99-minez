package Block.Storage;

import Block.Dto.BlockDto;
import com.sun.org.apache.xerces.internal.xs.StringList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class BlockPersistanceStorage
{
    private JavaPlugin plugin;

    public BlockPersistanceStorage(JavaPlugin plugin)
    {
        this.plugin = plugin;
    }


    public void store(Block block, Player player) throws IOException, InvalidConfigurationException {
        World world = block.getWorld();
        File file = new File(this.plugin.getDataFolder()+ "/Block/persistence/", world.getUID().toString()+".yml");
        YamlConfiguration configuration = new YamlConfiguration();
        List<HashMap<String, Object>> blockMapList = new ArrayList<>();
        HashMap<String, Object> hm = new HashMap<>();
        hm.put("x", block.getLocation().getX());
        hm.put("y", block.getLocation().getY());
        hm.put("z", block.getLocation().getZ());
        hm.put("player", player.getUniqueId().toString());

        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        if(!file.exists()){
            file.createNewFile();
            configuration.set("world", world.getUID().toString());
            configuration.set("blocks", blockMapList);
            configuration.save(file);
        }

        configuration.load(file);
        blockMapList = (List<HashMap<String, Object>>) configuration.get("blocks");

        if(null == configuration.get("blocks")){
            blockMapList = new ArrayList<>();
        }

        if(blockMapList.contains(hm)){
            blockMapList.remove(hm);
        }

        blockMapList.add(hm);
        configuration.set("blocks", blockMapList);
        configuration.save(file);
    }

    public List<BlockDto> getBlocks(World world) throws IOException, InvalidConfigurationException {
        List<BlockDto> blocks = new ArrayList<>();

        File file = new File(this.plugin.getDataFolder()+ "/Block/persistence/", world.getUID().toString()+".yml");
        YamlConfiguration configuration = new YamlConfiguration();
        if(!file.exists()){
            return blocks;
        }
        configuration.load(file);
        List<HashMap<String, Object>> blockMapList = (List<HashMap<String, Object>>) configuration.get("blocks");

        for(HashMap<String, Object> blockMap:blockMapList){
            if(null == blockMap){
                continue;
            }

            BlockDto blockDto = new BlockDto(
                    (double) blockMap.getOrDefault("x", 0),
                    (double) blockMap.getOrDefault("y", 0),
                    (double) blockMap.getOrDefault("z", 0),
                    (String) blockMap.getOrDefault("player", "Xsrf")
            );

            blocks.add(blockDto);
        }

        return blocks;
    }

    public static boolean hasAccess(Block block, Player player)
    {
        if(!BlockPersistanceStorage.isProtected(block)){
            return true;
        }

        List<MetadataValue> metas = block.getMetadata("player");

        for (MetadataValue metadataValue:metas){
            if(null == metadataValue.value()){
                continue;
            }
            if((metadataValue.value().toString()).equals(player.getUniqueId().toString())){
                continue;
            }

            return true;
        }
        return false;
    }

    public static boolean isProtected(Block block)
    {
        if(!block.hasMetadata("player")){
            return false;
        }

        return true;
    }

    public static Player getPlayer(Block block)
    {
        if(!isProtected(block)){
            return null;
        }

        List<MetadataValue> metas = block.getMetadata("player");

        for (MetadataValue metadataValue:metas){
            if(null == metadataValue){
                continue;
            }

            if(null == metadataValue.value()){
                continue;
            }

            return Bukkit.getPlayer(UUID.fromString(metadataValue.value().toString()));

        }
        return null;
    }

}
