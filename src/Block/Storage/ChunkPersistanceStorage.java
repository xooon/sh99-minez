package Block.Storage;

import Block.Dto.BlockDto;
import Block.Dto.ChunkDto;
import minez.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ChunkPersistanceStorage
{
    private JavaPlugin plugin;

    public ChunkPersistanceStorage(JavaPlugin plugin)
    {
        this.plugin = plugin;
    }

    public boolean addMember(Player player, String member) throws IOException, InvalidConfigurationException {
        Chunk chunk = player.getLocation().getChunk();
        ChunkDto chunkDto = this.getChunk(chunk);

        if(null == chunkDto){
            return false;
        }

        File file = new File(this.plugin.getDataFolder()+ "/Chunk/persistence/", Utils.getChunkUuId(chunk) +".yml");
        YamlConfiguration configuration = new YamlConfiguration();

        if(!file.exists()){
            return false;
        }



        configuration.load(file);

        List<String> members;

        if(null == configuration.get("member")){
            members = new ArrayList<>();
        }else {
            members = (List<String>) configuration.get("member");
        }

        if(members.contains(member)){
            return false;
        }

        members.add(member);
        configuration.set("x",  chunk.getX());
        configuration.set("z",  chunk.getZ());
        configuration.set("player",  player.getUniqueId().toString());
        configuration.set("world",  chunk.getWorld().getUID().toString());
        configuration.set("region",  chunkDto.getRegion());
        configuration.set("member", members);
        configuration.save(file);
        return true;
    }

    public boolean update(Player player, String region) throws IOException, InvalidConfigurationException {
        Chunk chunk = player.getLocation().getChunk();
        File file = new File(this.plugin.getDataFolder()+ "/Chunk/persistence/", Utils.getChunkUuId(chunk) +".yml");
        YamlConfiguration configuration = new YamlConfiguration();

        if(!file.exists()){
            return false;
        }

        configuration.load(file);

        configuration.set("x",  chunk.getX());
        configuration.set("z",  chunk.getZ());
        configuration.set("player",  player.getUniqueId().toString());
        configuration.set("world",  chunk.getWorld().getUID().toString());
        configuration.set("region",  region);
        configuration.save(file);
        return true;
    }

        //TODO: add chunk member for multiple protection like List<Player>
    public boolean store(Player player, boolean forceSave) throws IOException, InvalidConfigurationException {
        Chunk chunk = player.getLocation().getChunk();
        File file = new File(this.plugin.getDataFolder()+ "/Chunk/persistence/", Utils.getChunkUuId(chunk) +".yml");
        YamlConfiguration configuration = new YamlConfiguration();

        if(!file.getParentFile().exists()){
            file.getParentFile().mkdirs();
            file.getParentFile().setWritable(true);
            file.getParentFile().setReadable(true);
        }

        if(!file.exists()){
            file.createNewFile();
            configuration.set("x",  chunk.getX());
            configuration.set("z",  chunk.getZ());
            configuration.set("player",  player.getUniqueId().toString());
            configuration.set("world",  chunk.getWorld().getUID().toString());
            configuration.set("region",  null);
            configuration.save(file);
            return true;
        }

        if(!forceSave){
            return false;
        }

        configuration.load(file);

        configuration.set("x",  chunk.getX());
        configuration.set("z",  chunk.getZ());
        configuration.set("player",  player.getUniqueId().toString());
        configuration.set("world",  chunk.getWorld().getUID().toString());
        configuration.set("region",  null);
        configuration.save(file);
        return true;
    }

    public ChunkDto getChunk(Chunk chunk) throws IOException, InvalidConfigurationException {
        ChunkDto chunkDto;

        File file = new File(this.plugin.getDataFolder()+ "/Chunk/persistence/", Utils.getChunkUuId(chunk) +".yml");
        YamlConfiguration configuration = new YamlConfiguration();
        if(!file.exists()){
            return null;
        }
        configuration.load(file);

        //TODO: add regions to game
        chunkDto = new ChunkDto(
                (int) configuration.get("x"),
                (int) configuration.get("z"),
                (String) configuration.get("player"),
                (String) configuration.get("world"),
                (String) configuration.get("region")
        );

        if(!(null == configuration.get("member"))){
            for (String member:(List<String>)configuration.get("member")){
                chunkDto.addMember(member);
            }
        }


        return chunkDto;
    }

}
