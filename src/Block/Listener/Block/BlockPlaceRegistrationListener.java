package Block.Listener.Block;


import Block.Storage.BlockPersistanceStorage;
import minez.Main;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.metadata.MetadataValue;
import org.bukkit.Bukkit;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.plugin.Plugin;

import java.io.IOException;

public class BlockPlaceRegistrationListener implements Listener
{
    private BlockPersistanceStorage persistanceStorage;

    public BlockPlaceRegistrationListener(BlockPersistanceStorage persistanceStorage)
    {
        this.persistanceStorage = persistanceStorage;
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public Boolean onBlockPlace(BlockPlaceEvent event) throws IOException, InvalidConfigurationException {
        Block block = event.getBlockPlaced();

        Plugin plugin = Bukkit.getPluginManager().getPlugin(Main.PLUGIN);

        if (null == plugin){
            return false;
        }

        MetadataValue metadataValue = new FixedMetadataValue(plugin, event.getPlayer().getUniqueId().toString());

        block.setMetadata("player", metadataValue);

        if(
                block.getType().equals(Material.ACACIA_DOOR) ||
                block.getType().equals(Material.DARK_OAK_DOOR) ||
                block.getType().equals(Material.BIRCH_DOOR) ||
                block.getType().equals(Material.IRON_DOOR) ||
                block.getType().equals(Material.JUNGLE_DOOR) ||
                block.getType().equals(Material.OAK_DOOR) ||
                block.getType().equals(Material.SPRUCE_DOOR)
        ){
            Location doorUpLocation = block.getLocation();
            doorUpLocation.setY(doorUpLocation.getY()+1);
            doorUpLocation.getBlock().setMetadata("player", metadataValue);
            this.persistanceStorage.store(doorUpLocation.getBlock(), event.getPlayer());
        }

        this.persistanceStorage.store(block, event.getPlayer());

        return true;
    }
}
