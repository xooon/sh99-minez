package Block.Listener.Block;


import Block.Dto.ChunkDto;
import Block.Storage.ChunkPersistanceStorage;
import org.bukkit.ChatColor;
import org.bukkit.block.Block;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.MetadataValue;

import java.io.IOException;
import java.util.List;

public class BlockPlayerInteractionListener implements Listener
{
    private ChunkPersistanceStorage chunkPersistanceStorage;

    public BlockPlayerInteractionListener(ChunkPersistanceStorage chunkPersistanceStorage)
    {
        this.chunkPersistanceStorage = chunkPersistanceStorage;
    }

    @EventHandler(priority = EventPriority.LOW)
    public Boolean onInteract(PlayerInteractEvent event) throws IOException, InvalidConfigurationException {
        Block block = event.getClickedBlock();

        if(null == block){
            return true;
        }

        if (block.hasMetadata("player")){
            List<MetadataValue> metas = block.getMetadata("player");

            for (MetadataValue metadataValue:metas){
                if(null == metadataValue){
                    continue;
                }

                if(null == metadataValue.value()){
                    continue;
                }

                if(!(metadataValue.value()).equals(event.getPlayer().getUniqueId().toString())){
                    continue;
                }

                return true;
            }
        }



        ChunkDto chunkDto = this.chunkPersistanceStorage.getChunk(block.getChunk());

        if(null == chunkDto){
            return true;
        }

        if(!chunkDto.getMembers().contains(event.getPlayer().getUniqueId().toString()) && !chunkDto.getPlayer().equals(event.getPlayer().getUniqueId().toString())){
            event.getPlayer().sendMessage(ChatColor.RED + "You are not authorized to interact with this block.");
            event.setCancelled(true);
            return true;
        }

        return true;
    }
}
