package Block.Command;

import Block.Dto.ChunkDto;
import Block.Storage.ChunkPersistanceStorage;
import Economy.Storage.EconomyPeristanceStorage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.entity.Player;

import java.io.IOException;

public class PlotCommand implements CommandExecutor
{
    private ChunkPersistanceStorage chunkPersistanceStorage;
    private EconomyPeristanceStorage economyPeristanceStorage;

    public PlotCommand(
            ChunkPersistanceStorage chunkPersistanceStorage,
            EconomyPeristanceStorage economyPeristanceStorage
    )
    {
        this.chunkPersistanceStorage = chunkPersistanceStorage;
        this.economyPeristanceStorage = economyPeristanceStorage;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String s, String[] args)
    {
        if(!(sender instanceof Player)){
            return false;
        }

        Player player = (Player) sender;

        if(args.length <= 0){
            try {
                ChunkDto chunkDto = this.chunkPersistanceStorage.getChunk(player.getLocation().getChunk());

                if(null == chunkDto){
                    player.sendMessage(ChatColor.YELLOW + "Plot information" + ChatColor.GREEN + " FREE");
                    player.sendMessage(ChatColor.YELLOW + "World: " + ChatColor.WHITE + player.getWorld().getName());
                    player.sendMessage(ChatColor.YELLOW + "X: " + ChatColor.WHITE + player.getLocation().getChunk().getX());
                    player.sendMessage(ChatColor.YELLOW + "Z: " + ChatColor.WHITE + player.getLocation().getChunk().getZ());
                    player.sendMessage(ChatColor.YELLOW + "Owner: " + ChatColor.RED + "-");
                    player.sendMessage(ChatColor.YELLOW + "Region: " + ChatColor.LIGHT_PURPLE + "-");
                    return true;
                }

                player.sendMessage(ChatColor.YELLOW + "Plot information");
                player.sendMessage(ChatColor.YELLOW + "World: " + ChatColor.WHITE + chunkDto.getWorld());
                player.sendMessage(ChatColor.YELLOW + "X: " + ChatColor.WHITE + chunkDto.getX());
                player.sendMessage(ChatColor.YELLOW + "Z: " + ChatColor.WHITE + chunkDto.getZ());
                player.sendMessage(ChatColor.YELLOW + "Owner: " + ChatColor.RED + chunkDto.getPlayer());
                player.sendMessage(ChatColor.YELLOW + "Region: " + ChatColor.LIGHT_PURPLE + chunkDto.getRegion());
                player.sendMessage(ChatColor.GREEN + "Members");

                for (String infoMember:chunkDto.getMembers()){
                    player.sendMessage(ChatColor.YELLOW + "Member: " + ChatColor.LIGHT_PURPLE + infoMember);

                }

                return true;
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InvalidConfigurationException e) {
                e.printStackTrace();
            }
        }

        Chunk chunk = player.getLocation().getChunk();

        String operator = args[0];

        switch (operator){
            case "buy":
                try {
                    this.economyPeristanceStorage.add(player, -25);

                    if(!this.chunkPersistanceStorage.store(player, false)){
                        player.sendMessage(ChatColor.RED + "This chunk is already owned by another player.");
                        return true;
                    }

                    player.sendMessage(
                            ChatColor.WHITE + "You are the " +
                                    ChatColor.RED + "Owner" +
                                    ChatColor.WHITE + " of this " +
                                    ChatColor.DARK_GREEN + "Plot" +
                                    ChatColor.WHITE + " now."
                    );
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return false;
                } catch (InvalidConfigurationException e) {
                    e.printStackTrace();
                }
            case "add":
                try {
                    if(args.length <= 1){
                        return true;
                    }

                    String member = args[1];

                    Player memberPlayer = Bukkit.getPlayer(member);

                    if(null == memberPlayer){
                        player.sendMessage(ChatColor.RED + "Player could not be found.");
                        return true;
                    }

                    ChunkDto chunkDto = this.chunkPersistanceStorage.getChunk(chunk);

                    if(null == chunkDto){
                        player.sendMessage(ChatColor.RED + "This chunk is not owned by a player yet.");
                        return true;
                    }
                    //TODO: add regiona protection (store regions (get region, if player == region.member)
                    if(!this.chunkPersistanceStorage.addMember(player, memberPlayer.getUniqueId().toString())){
                        player.sendMessage(ChatColor.RED + "Could not add this player as a member to this plot.");
                        return true;
                    }

                    player.sendMessage(ChatColor.GREEN + "Player has been added to your chunk.");
                    return true;
                } catch (InvalidConfigurationException e) {
                    e.printStackTrace();
                    return true;
                } catch (IOException e) {
                    e.printStackTrace();
                    return true;
                }
            case "region":

                if(args.length <= 1){
                    return true;
                }

                String region = args[1];

                try {
                    ChunkDto chunkDto = this.chunkPersistanceStorage.getChunk(chunk);

                    if(null == chunkDto){
                        player.sendMessage(ChatColor.RED + "This chunk is not owned by a player yet.");
                        return true;
                    }

                    if(!chunkDto.getPlayer().equals(player.getUniqueId().toString())){
                        player.sendMessage(ChatColor.RED + "You are not the owner of this chunk.");

                        return true;
                    }

                    if(!this.chunkPersistanceStorage.update(player, region)){
                        player.sendMessage(ChatColor.RED + "There was a problem while persisting chunk's data.");
                        return true;
                    }

                    player.sendMessage(ChatColor.WHITE + "This chunk has been added to region " + ChatColor.GREEN + region + ChatColor.WHITE + ".");
                    return true;
                }catch (IOException e){
                    return true;
                } catch (InvalidConfigurationException e) {
                    e.printStackTrace();
                    return true;
                }
        }

        player.sendMessage(ChatColor.RED + "Operator '" + operator + "' is not provided in this command.");
        return true;
    }
}
