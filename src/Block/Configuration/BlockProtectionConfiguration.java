package Block.Configuration;

import config.IConfigurable;
import org.bukkit.World;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class BlockProtectionConfiguration implements IConfigurable
{
        @Override
        public String getName() {
            return "block-protection";
        }

        @Override
        public FileConfiguration getConfiguration() {
            FileConfiguration config = new YamlConfiguration();

            config.set("enabled", true);
            config.set("persisted-worlds", new ArrayList<>(Arrays.asList("main")));

            return config;
        }

}